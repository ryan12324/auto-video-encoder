#!/usr/bin/env ruby
# encoding: utf-8

require "rubygems"
require "bunny"
require "streamio-ffmpeg"
require "json"

STDOUT.sync = true

conn = Bunny.new("amqp://guest:guest@rabbitmq")

sleep 10 #sleep to wait for the rabbitmq server

conn.start
puts "connected to RabbitMQ"

ch = conn.create_channel
ch.prefetch(0);
jobq = ch.queue("encoder.encode.jobs", :durable => true, :auto_delete => false)
failureq = ch.queue("encoder.encode.failure", :durable => true, :auto_delete => false)
successq = ch.queue("encoder.encode.success", :durable => true, :auto_delete => false)



jobq.subscribe(:block => true) do |delivery_info, properties, payload|
  begin
      movieData = JSON.parse(payload)
      movie = FFMPEG::Movie.new(movieData['source'])

      if(!movie.valid?)
        puts "Sending error to failure queue"
        failureq.publish(payload, :routing_key => failureq.name)
        return false
      end

      options = {
        video_codec: movieData['options']['codec']['video'],
        video_bitrate: movieData['options']['bitrate']['video'],
        audio_codec: movieData['options']['codec']['audio']
        }
      transcoded = movie.transcode(movieData['output'], options){ |progress| puts progress }

      if (!transcoded.valid?)
        puts "Sending error to failure queue"
        failureq.publish(payload, :routing_key => failureq.name)
        return false
      end
        puts "Encoding successful"
        successq.publish(payload, :routing_key => successq.name)
  rescue
      puts "Sending error to failure queue"
      failureq.publish(payload, :routing_key => failureq.name)
  end
end
