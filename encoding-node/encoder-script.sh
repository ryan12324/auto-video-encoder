#!/usr/bin/env bash
function checkExit {
    "$@"
    local status=$?
    if [ $status -ne 0 ]; then
        echo "error with $1" >&2
    else
        rm ./busy
    fi
    return $status
}
touch ./busy;
checkExit ffmpeg -y -i $1 -c:v libvpx -b:v 1M -c:a libvorbis $2;
